function oneThroughTwenty() {
    
   /* Your code goes below
   Write a for or a while loop
   console.log() the result*/

   console.log('oneThroughTwenty')
   let number = 1
   for(number; number <= 20; number++){
     console.log(number)
   }
    
}
//call function oneThroughTwenty
oneThroughTwenty()

function evensToTwenty() {

   /* Your code goes below
   Write a for or a while loop
   console.log() the result */
  
   console.log('evensThroughTwenty')
   let number = 2
   while(number <= 20){
     console.log(number)
     number += 2
   }

}

//call function evensToTwenty
evensToTwenty()

function oddsToTwenty() {
    
  /* Your code goes below
  Write a for or a while loop
  console.log() the result */

  console.log('oddsThroughTwenty')

  let number = 1
  while(number <= 20){
    console.log(number)
    number += 2
  }
   
}

//call function oddsToTwenty
oddsToTwenty()

function multiplesOfFive() {
    
  /* Your code goes below
  Write a for or a while loop
  console.log() the result */

console.log('multiplesOfFive')
let number = 5

while(number <= 100){
  console.log(number)
  number += 5
}
}

//call function multiplesOfFive
multiplesOfFive()


function squareNumbers() {
    
 /* Your code goes below
  Write a for or a while loop
  console.log() the result */
  console.log('squareNumbers')

  let index = 1
  while(index <= 10){
    let number = index * index
    console.log(number)
    index++
  }
  }

//call function squareNumbers
squareNumbers()

function countingBackwards() {
    
  /* Your code goes below
  Write a for or a while loop
  console.log() the result */

  console.log('countingBackwards')
  
  let number = 20
  while(number >= 1){
    console.log(number)
    number --
  }

}

//call function countingBackwards
countingBackwards()

function evenNumbersBackwards() {
    
 /* Your code goes below
  Write a for or a while loop
  console.log() the result */

  console.log('evenNumbersBackwards')
   let number = 20
   while(number >= 1){
     console.log(number)
     number -= 2
   }
}

//call function evenNumbersBackwards
evenNumbersBackwards()

function oddNumbersBackwards() {
    
 /* Your code goes below
  Write a for or a while loop
  console.log() the result */

  console.log('oddNumbersBackwards')

  let number = 19
  while(number >= 1){
    console.log(number)
    number -= 2
  }
}

//call function oddNumbersBackwards
oddNumbersBackwards()


function multiplesOfFiveBackwards() {
    
 /* Your code goes below
  Write a for or a while loop
  console.log() the result */

  console.log('ultiplesOfFiveBackwards')
let number = 100

while(number >= 1){
  console.log(number)
  number -= 5
}
}

//call function multiplesOfFiveBackwards
multiplesOfFiveBackwards()

function squareNumbersBackwards() {
    
   /* Your code goes below
  Write a for or a while loop
  console.log() the result */

  console.log('squareNumbersBackwards')

  let index = 10
  while(index >= 1){
    let number = index * index
    console.log(number)
    index--
  }
}

//call function squareNumbersBackwards
squareNumbersBackwards()